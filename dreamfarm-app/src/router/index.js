import Vue from 'vue';
import VueRouter from 'vue-router';
import Main from '../views/index.vue';
import ControlDetail from '../views/ControlDetail.vue';
import CubeManage from '../views/CubeManage.vue';
import Setting from '../views/Setting.vue';
import Data from '../views/Data.vue';
import DataDetail from '../views/DataDetail.vue';
import Login from '../views/login.vue';

Vue.use(VueRouter);

const routes = [
  {
    path: '/index',
    name: 'index',
    component: Main,
  },
  {
    path: '/',
    name: 'login',
    component: Login,
  },
  {
    path: '/ControlDetail',
    name: 'ControlDetail',
    component: ControlDetail,
  },
  {
    path: '/CubeManage',
    name: 'CubeManage',
    component: CubeManage,
  },
  {
    path: '/Setting',
    name: 'Setting',
    component: Setting,
  },
  {
    path: '/Data',
    name: 'Data',
    component: Data,
  },
  {
    path: '/DataDetail',
    name: 'DataDetail',
    component: DataDetail,
  },
];

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes,
});

router.beforeEach((to, from, next) => {
  // 라우터 변경 전 실행될 코드
  const token = Vue.$cookies.get("user_token");
  const userID= Vue.$cookies.get("user_id")
  
  if (!token && to.path !== '/') {
    next('/');
  } else {
    next(); // 라우터 변경
  }
    
  if (!userID && to.path !== '/') {
    next('/');
  } else {
    next(); // 라우터 변경
  }
});

export default router;