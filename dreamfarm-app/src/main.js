import Vue from 'vue';
import Vuex from 'vuex';
import App from './App.vue';
import router from './router';
import store from './store';
import ElementUI from 'element-ui'
import locale from 'element-ui/lib/locale/lang/en'
import 'element-ui/lib/theme-chalk/index.css';
import VueApexCharts from 'vue-apexcharts';
import axios from 'axios';
import VueCookies from "vue-cookies";


Vue.prototype.$Axios = axios;
Vue.config.productionTip = false;
Vue.use(VueApexCharts);
Vue.use(Vuex)
Vue.use(VueCookies);

Vue.$cookies.config("1d");

Vue.component('apexchart', VueApexCharts);
Vue.use(ElementUI, { locale });
new Vue({
  router,
  store,
  render: (h) => h(App),
}).$mount('#app');
